import AbstractTextInput from "../../component/AbstractTextInput";
import { guid } from "../../util/guid";

export default class TextInputComponent extends AbstractTextInput {

    private rootElem: HTMLDivElement;
    private inputElem: HTMLInputElement;
    private labelElem: HTMLLabelElement;

    constructor() {
        super();

        const id = `input-${guid()}`;
        const elem = document.createElement('div');
        elem.className = this.rootClassName;
        elem.innerHTML = `\
            <input type="text" id="${id}" class="mdc-text-field__input">\
            <label class="mdc-floating-label" for="${id}">--</label>\
            <div class="mdc-line-ripple"></div>`;

        this.attachShadow({ mode: 'open' }).appendChild(elem);
        this.rootElem = elem;
        this.inputElem = elem.firstElementChild as HTMLInputElement;
        // initially, there is no label
        this.labelElem = (this.inputElem.nextSibling as HTMLLabelElement).also((it) => it.remove());
    }

    static get observedAttributes() {
        return ['label', 'enabled', 'value', 'required', 'pattern'];
    }

    public attributeChangedCallback(name: string, oldValue: any, newValue: any) {
        switch (name) {
            case 'enabled':
                this.rootElem.className = this.rootClassName;
                break;
            case 'label':
                if (oldValue === null) {
                    this.rootElem.insertAfter(this.labelElem, this.inputElem);
                    this.labelElem.textContent = newValue;
                } else {
                    this.labelElem.remove();
                }

                this.rootElem.className = this.rootClassName;
                break;
            case 'value':
                this.inputElem.value = newValue;
                this.labelElem.className = this.labelClassName;
                break;
            case 'required':
            case 'pattern':
                if (newValue === null) {
                    this.inputElem.removeAttribute(name);
                } else {
                    this.inputElem.setAttribute(name, newValue);
                }
                break;
        }
    }

    public isValid(): boolean {
        return this.inputElem.validity.valid;
    }

    private get rootClassName(): string {
        let clsName = 'mdc-text-field';

        if (this.getAttribute('label') === null) {
            clsName += ' mdc-text-field--no-label';
        }

        if (!this.hasAttribute('enabled')) {
            clsName += ' mdc-text-field--disabled';
        }

        return clsName;
    }

    private get labelClassName() {
        let clsName = 'mdc-floating-label';

        if (this.getAttribute('value') !== null) {
            clsName += ' mdc-floating-label--float-above';
        }

        return clsName;
    }

}

window.customElements.define('text-input', TextInputComponent);
