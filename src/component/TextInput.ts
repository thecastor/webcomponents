import { Nullable } from "../util/lang";
import InputControl from "./InputControl";

export default interface TextInput extends InputControl<string> {

    label: Nullable<string>;

}
