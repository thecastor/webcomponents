import { Nullable } from "../util/lang";

export default interface InputControl<T> {

    isValid(): boolean;

    value: Nullable<T>;

    enabled: boolean;

}
