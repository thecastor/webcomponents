export default interface Validator<T> {

    validate(input: T): boolean;

}