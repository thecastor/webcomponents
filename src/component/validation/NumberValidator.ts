import { NumberValidationFunc } from "./ValidationFunc";
import Validator from "./Validator";

export default class NumberValidator implements Validator<number> {

    private readonly validator: NumberValidationFunc;

    constructor(validate: NumberValidationFunc) {
        this.validator = validate;
    }

    public validate(input: number): boolean {
        return this.validator(input);
    }

}