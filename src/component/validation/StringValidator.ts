import { StringValidationFunc } from "./ValidationFunc";
import Validator from "./Validator";

export default class StringValidator implements Validator<string> {

    private readonly validator: StringValidationFunc;

    constructor(validate: StringValidationFunc) {
        this.validator = validate;
    }

    public validate(input: string): boolean {
        return this.validator(input);
    }

}