import {Dict} from '../../util/lang';

export type ValidationFunc<T> = (input: T) => boolean;

export type StringValidationFunc = ValidationFunc<string>;
export type NumberValidationFunc = ValidationFunc<number>;

const naturalNumberRegex: RegExp = /^[\-\+]?\d+(e\d+)?$/;
const positiveNaturalNumberRegex: RegExp = /^\+?\d+(e\d+)?$/;
const negativeNaturalNumberRegex: RegExp = /^\-?\d+(e\d+)?$/;

export const NoOp = (_: any) => true;

export const NumberValidation: Readonly<Dict<NumberValidationFunc>> = {

    NaturalNumber: (input: number | string): boolean => {
        return naturalNumberRegex.test("" + input);
    },

    PositiveNaturalNumber: (input: number | string): boolean => {
        return +input > 0 && positiveNaturalNumberRegex.test("" + input);
    },

    NegativeNaturalNumber: (input: number | string): boolean => {
        return +input < 0 && negativeNaturalNumberRegex.test("" + input);
    }

};

// see https://www.regular-expressions.info/email.html
export const emailRegex: RegExp = /^(?=[a-z0-9@.!#$%&'*+/=?^_`{|}~-]{6,254}$)(?=[a-z0-9.!#$%&'*+/=?^_`{|}~-]{1,64}@)[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:(?=[a-z0-9-]{1,63}\.)[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?=[a-z0-9-]{1,63}$)[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;

export const StringValidation: Readonly<Dict<StringValidationFunc>> = {

    Email: (input: string): boolean => {
        return emailRegex.test(input);
    }

};
