import { Nullable } from "../util/lang";
import TextInput from "./TextInput";

export default class AbstractTextInput extends HTMLElement implements TextInput {

    public get label(): string | null {
        return this.getAttribute('label');
    }

    public set label(label: string | null) {
        if (label !== null) {
            this.setAttribute('label', label);
        } else {
            this.removeAttribute('label');
        }
    }

    public isValid(): boolean {
        throw new Error("Method not implemented.");
    }

    public get value(): Nullable<string> {
        return this.getAttribute('value');
    }

    public get enabled(): boolean {
        return this.hasAttribute('enabled');
    }

    public set enabled(enabled: boolean) {
        if (enabled) {
            this.setAttribute('enabled', 'true');
        } else {
            this.removeAttribute('enabled');
        }
    }

}
