export type Nullable<T> = T | null;

export interface Dict<T> { [key: string]: T; }

export type Consumer<T> = (it: T) => void;
