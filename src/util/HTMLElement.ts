import { Consumer } from "./lang";

declare global {
    interface HTMLElement {
        also(f: Consumer<HTMLElement>): this;
    }

    interface Node {
        insertAfter<T extends Node>(newNode: T, referenceNode: Node | null): T;
    }
}

HTMLElement.prototype.also = function (f: Consumer<HTMLElement>): HTMLElement {
    f(this);

    return this;
};

Node.prototype.insertAfter = function <T extends Node>(newNode: T, referenceNode: Node | null): T {
    const before = referenceNode && referenceNode.nextSibling || null;

    return this.insertBefore(newNode, before);
};
